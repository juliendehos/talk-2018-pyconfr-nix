
# Exemple 3 : extension C++


## Sans Nix

```
sudo apt install libboost-all-dev libeigen3-dev pkg-config
source myenv/bin/activate
pip install -r requirements.txt
pip install .
randmat3.py
```


## Avec Nix

### Exécution dans un nix-shell

```
nix-shell --run randmat3.py
```


### Construction avec nix-build

```
nix-build
```

