#include <Eigen/Dense>
#include <iostream>
void print_randmat() {
    Eigen::MatrixXd m = Eigen::MatrixXd::Random(2, 3);
    std::cout << m << std::endl;
}

#include <boost/python.hpp>
BOOST_PYTHON_MODULE( randmat ) {
    boost::python::def("print_randmat", &print_randmat);
}

