{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {} } :

with pkgs; python3Packages.buildPythonPackage {
  name = "exemple_3";
  src = ./.;
  buildInputs = [
    eigen3_3
    pkgconfig
    python3Packages.boost
    python3Packages.pkgconfig
  ];
}

