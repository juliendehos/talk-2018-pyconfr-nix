from setuptools import setup, Extension
import pkgconfig

exts = [Extension('randmat',
        extra_compile_args=[pkgconfig.cflags('eigen3')],
        libraries=['boost_python3'],
        sources=['randmat.cpp'])]

setup(name='exemple_3',
      version='0.1.0',
      ext_modules=exts,
      scripts=['randmat3.py'])
