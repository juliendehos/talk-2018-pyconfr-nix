{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {} }:

with pkgs;
python3Packages.buildPythonPackage {
  name = "projet1";
  src = ./.;
  propagatedBuildInputs = [
    #(python3.withPackages(ps: with ps; [ numpy ]))
    python3Packages.numpy
  ];
}

