with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {};

stdenv.mkDerivation {

  name = "pyconfr-2018-nix";

  buildInputs = [
    pkgconfig
    eigen3_3
    boost
    (python3.withPackages(ps: with ps; [ 
      boost
      pip 
      virtualenv 
    ]))
  ];

  shellHook = ''
    unset SOURCE_DATE_EPOCH;
  '';

  src = ./.;

  installPhase = ''
    mkdir -p $out
  '';
}

