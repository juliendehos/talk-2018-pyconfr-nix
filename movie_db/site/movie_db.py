
import mymath
import numpy
import psycopg2


DATABASE_URL = "host=localhost port=5432 dbname=moviedb user=movieuser password='toto'"


def get_actors():
    conn = psycopg2.connect(DATABASE_URL)
    cur = conn.cursor()
    cur.execute("SELECT actor_name \
        FROM movie \
        GROUP BY actor_name \
        HAVING count(actor_name) > 10 \
        ORDER BY actor_name;")
    res = []
    for record in cur.fetchall():
        res.append(record[0])
    conn.commit()
    cur.close()
    conn.close()
    return res


def get_years(name):
    conn = psycopg2.connect(DATABASE_URL)
    cur = conn.cursor()
    cur.execute("SELECT * FROM movie WHERE lower(actor_name) LIKE \
            lower(%s);", (name,))
    res = []
    for record in cur.fetchall():
        res.append(record[1])
    conn.commit()
    cur.close()
    conn.close()
    return res


if __name__ == "__main__":

    import sys

    print(sys.argv)
    if len(sys.argv) != 2:
        actors = get_actors()
        for actor in actors:
            print(actor)
    else:
        actor = sys.argv[1]
        years = get_years(actor)
        print(actor)
        print(years)
        if years:
            years_arr = numpy.asarray(years, dtype=int)
            d0, d1, hist = mymath.myhist(years_arr)
            print(d0)
            print(d1)
            print(hist)

